import uvicorn
from fastapi import FastAPI, HTTPException
from starlette.middleware.cors import CORSMiddleware
from models.database import database

from components.xml_reader import *
from routers import users

data = XmlReader('data_xml/Сотрудники.xml')
data.get_actual_birth()

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users.router)


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/")
async def hello_world():
    return {"Hello": "World"}


@app.get("/api/birthdays/")
def get_persons():
    return data.get_persons_list()


@app.get("/api/links/")
def get_links():
    links_dict = {}
    for index, value in enumerate(data.get_persons_list()):
        alias = value['id'][1:-1]
        links_dict[index] = '/birthdays/' + alias
    return links_dict


@app.get("/api/birthdays/{id}")
def get_person(id):
    true_id = '{' + id + '}'
    actual_person = {}
    for item in data.get_persons_list():
        if item['id'] == true_id:
            actual_person = item
    if len(actual_person) > 0:
        return actual_person
    else:
        raise HTTPException(status_code=404, detail="Item not found")


if __name__ == '__main__':
    uvicorn.run(app)
#     data = XmlReader('data_xml/Сотрудники.xml')
#     data.get_actual_birth()
#     print(data.get_persons_list())
