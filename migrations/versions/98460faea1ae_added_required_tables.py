"""Added required tables

Revision ID: 98460faea1ae
Revises: 435a50c093fd
Create Date: 2022-06-25 20:00:44.234579

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '98460faea1ae'
down_revision = '435a50c093fd'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'admin')
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('admin', sa.BOOLEAN(), nullable=False))
    # ### end Alembic commands ###
